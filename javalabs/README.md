Softbinator Week 1 & 2 Application - Alin Velea


**Properties**
There are 2 configuration files: one for dev and one for production purposes. The active
profile can be changed inside **application.properties** by changing *spring.profiles.active* to prod/dev



**Requests**

Students should contain the following fields:
*Integer*: id
*String*: First name
*String*: Last name
*Long*: GPA
*String*: Prefered course
*Integer*: Current year

*Endpoints*: /student/...  
1) *POST* save - expects a student JSON which will be saved to a file
2) *GET* get-all - returns the list with all students
3) *GET* {id} - returns the student with this id
4) *DELETE* {id} - deletes the student with this id
5) *DELETE* all - deletes all students
6) *PUT* {id} - updates the student with this id (ID, lastname and firstName can't be edited)



Teachers should contain the following fields:
*Integer*: id
*String*: First name
*String*: Last name
*String*: Course
*Boolean*: Bad teacher

*Endpoints*: /teacher/...  
1) *POST* save - expects a teacher JSON which will be saved to a file
2) *GET* get-all - returns the list with all teachers
3) *GET* {id} - returns the teacher with this id
4) *DELETE* {id} - deletes the teacher with this id
5) *DELETE* all - deletes all teachers
6) *PUT* {id} - updates the teacher with this id (ID, lastname and firstName can't be edited)
