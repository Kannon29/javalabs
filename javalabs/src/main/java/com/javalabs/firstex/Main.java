package com.javalabs.firstex;

import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Person iohannis = new Person(1, "Iohannis");
        Person dancila = new Person(2, "Dancila");
        Programmer alinu = new Programmer(3, "Alinu", true);

        FileOutputStream fs = new FileOutputStream("bossy.txt");
        ObjectOutputStream outputStream = new ObjectOutputStream(fs);

        outputStream.writeObject(iohannis);
        outputStream.writeObject(dancila);
        outputStream.writeObject(alinu);

        outputStream.close();
        fs.close();

        FileInputStream fis = new FileInputStream("bossy.txt");
        ObjectInputStream inputStream = new ObjectInputStream(fis);

        Person p1 = (Person) inputStream.readObject();
        Person p2 = (Person) inputStream.readObject();
        Programmer p3 = (Programmer) inputStream.readObject();

        System.out.println(p1.toString());
        System.out.println(p2.toString());
        System.out.println(p3.toString());
    }
}
