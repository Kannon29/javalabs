package com.javalabs.firstex;

public class Programmer extends Person {

    private boolean pascalMaster;

    public Programmer(final int id, final String name, final boolean pascalMaster) {
        super(id, name);
        this.pascalMaster = pascalMaster;
    }

    public boolean isPascalMaster() {
        return pascalMaster;
    }

    public void setPascalMaster(boolean pascalMaster) {
        this.pascalMaster = pascalMaster;
    }

    @Override
    public String toString() {
        return "Programmer{" +
                "pascalMaster=" + pascalMaster +
                "} " + super.toString();
    }
}
