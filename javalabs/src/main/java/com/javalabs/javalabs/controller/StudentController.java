package com.javalabs.javalabs.controller;

import com.javalabs.javalabs.dtos.Student;
import com.javalabs.javalabs.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @PostMapping("/save")
    public ResponseEntity<?> addStudent(@RequestBody Student student) {
        if (studentService.exists(student.getId())) {
            return new ResponseEntity<>("Already Exists", HttpStatus.CONFLICT);
        }
        studentService.addStudent(student);
        return new ResponseEntity<>("Student saved", HttpStatus.OK);
    }

    @GetMapping("/get-all")
    public ResponseEntity<?> getAllStudents() {
        List<Student> students = studentService.getAll();
        if (students == null) {
            return new ResponseEntity<>("Bad Request", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(students, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable Integer id) {
        if (studentService.exists(id)) {
            return new ResponseEntity<>(studentService.getStudent(id), HttpStatus.OK);
        }

        return new ResponseEntity<>("Student doesn't exist", HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteById(@PathVariable Integer id) {
        if (studentService.exists(id)) {
            return new ResponseEntity<>(studentService.removeStudent(id), HttpStatus.OK);
        }

        return new ResponseEntity<>("Student doesn't exist", HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/all")
    public ResponseEntity<String> deleteAll() {
        try {
            studentService.removeAll();
            return new ResponseEntity<>("Action performed", HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Something bad happened", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateStudent(@PathVariable Integer id, @RequestBody Student student) {
        if (studentService.exists(id)) {
            Student currentStudent = studentService.getStudent(id);
            if (student.getCurrentYear() != null) {
                currentStudent.setCurrentYear(student.getCurrentYear());
            }
            if (student.getGpa() != null) {
                currentStudent.setGpa(student.getGpa());
            }
            if (student.getPrefCourse() != null) {
                currentStudent.setPrefCourse(student.getPrefCourse());
            }
            studentService.removeStudent(id);
            studentService.addStudent(currentStudent);
            return new ResponseEntity<>("Student updated", HttpStatus.OK);
        }

        return new ResponseEntity<>("Student doesn't exist", HttpStatus.BAD_REQUEST);
    }
}
