package com.javalabs.javalabs.controller;

import com.javalabs.javalabs.dtos.Student;
import com.javalabs.javalabs.dtos.Teacher;
import com.javalabs.javalabs.service.StudentService;
import com.javalabs.javalabs.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/teacher")
public class TeacherController {

    @Autowired
    private TeacherService teacherService;

    @PostMapping("/save")
    public ResponseEntity<?> addTeacher(@RequestBody Teacher teacher) {
        if (teacherService.exists(teacher.getId())) {
            return new ResponseEntity<>("Already Exists", HttpStatus.CONFLICT);
        }
        teacherService.addTeacher(teacher);
        return new ResponseEntity<>("Teacher saved", HttpStatus.OK);
    }

    @GetMapping("/get-all")
    public ResponseEntity<?> getAllTeachers() {
        List<Teacher> teachers = teacherService.getAll();
        if (teachers == null) {
            return new ResponseEntity<>("Bad Request", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(teachers, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable Integer id) {
        if (teacherService.exists(id)) {
            return new ResponseEntity<>(teacherService.getTeacher(id), HttpStatus.OK);
        }

        return new ResponseEntity<>("Teacher doesn't exist", HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteById(@PathVariable Integer id) {
        if (teacherService.exists(id)) {
            return new ResponseEntity<>(teacherService.removeTeacher(id), HttpStatus.OK);
        }

        return new ResponseEntity<>("Teacher doesn't exist", HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/all")
    public ResponseEntity<String> deleteAll() {
        try {
            teacherService.removeAll();
            return new ResponseEntity<>("Action performed", HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Something bad happened", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateStudent(@PathVariable Integer id, @RequestBody Teacher teacher) {
        if (teacherService.exists(id)) {
            Teacher currentTeacher = teacherService.getTeacher(id);
            if (teacher.getCourse() != null) {
                currentTeacher.setCourse(teacher.getCourse());
            }
            if (teacher.isBadTeacher() != null) {
                currentTeacher.setBadTeacher(teacher.isBadTeacher());
            }
            teacherService.removeTeacher(id);
            teacherService.addTeacher(currentTeacher);
            return new ResponseEntity<>("Teacher updated", HttpStatus.OK);
        }

        return new ResponseEntity<>("Teacher doesn't exist", HttpStatus.BAD_REQUEST);
    }
}
