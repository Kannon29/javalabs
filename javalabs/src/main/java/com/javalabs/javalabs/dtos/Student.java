package com.javalabs.javalabs.dtos;

public class Student extends Person {
    private Long gpa;
    private String prefCourse;
    private Integer currentYear;

    public Student(int id, String firstName, String lastName, Long gpa, String prefCourse,
                   Integer currentYear) {
        super(id, firstName, lastName);
        this.gpa = gpa;
        this.prefCourse = prefCourse;
        this.currentYear = currentYear;
    }

    public Long getGpa() {
        return gpa;
    }

    public void setGpa(Long gpa) {
        this.gpa = gpa;
    }

    public String getPrefCourse() {
        return prefCourse;
    }

    public void setPrefCourse(String prefCourse) {
        this.prefCourse = prefCourse;
    }

    public Integer getCurrentYear() {
        return currentYear;
    }

    public void setCurrentYear(Integer currentYear) {
        this.currentYear = currentYear;
    }

    @Override
    public String toString() {
        return "Student{" +
                "gpa=" + gpa +
                ", prefCourse='" + prefCourse + '\'' +
                ", currentYear=" + currentYear +
                "} " + super.toString();
    }
}
