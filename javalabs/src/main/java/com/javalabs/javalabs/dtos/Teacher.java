package com.javalabs.javalabs.dtos;

public class Teacher extends Person {
    private String course;
    private Boolean badTeacher;


    public Teacher(int id, String firstName, String lastName, String course, Boolean badTeacher) {
        super(id, firstName, lastName);
        this.course = course;
        this.badTeacher = badTeacher;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public Boolean isBadTeacher() {
        return badTeacher;
    }

    public void setBadTeacher(Boolean badTeacher) {
        this.badTeacher = badTeacher;
    }
}
