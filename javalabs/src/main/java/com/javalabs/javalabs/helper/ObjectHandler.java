package com.javalabs.javalabs.helper;

import com.javalabs.javalabs.dtos.Person;

import java.io.*;

public class ObjectHandler {
    private static final Object object = new Object();
    private static volatile ObjectHandler instance = null;

    private ObjectHandler() {

    }

    public static ObjectHandler getInstance() {
        if (instance != null) {
            return instance;
        }

        synchronized (object) {
            if (instance == null) {
                instance = new ObjectHandler();
            }
            return instance;
        }
    }

    public <T extends Person> void writePerson(ObjectOutputStream os, T person) throws IOException {
        os.writeObject(person);
    }

    public <T extends Person> T readPerson(ObjectInputStream is)
            throws IOException, ClassNotFoundException {
        return (T) is.readObject();
    }

}
