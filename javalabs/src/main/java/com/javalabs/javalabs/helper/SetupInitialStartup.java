package com.javalabs.javalabs.helper;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public class SetupInitialStartup implements ApplicationListener<ContextRefreshedEvent> {
    private boolean alreadySetup = false;

    @Value("${teacher.path}")
    private String teacherPath;

    @Value("${student.path}")
    private String studentPath;

    public void onApplicationEvent(final ContextRefreshedEvent event) {
        try {
            Files.createDirectories(Paths.get(teacherPath));
            Files.createDirectories(Paths.get(studentPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        alreadySetup = true;
    }
}
