package com.javalabs.javalabs.service;

import com.javalabs.javalabs.dtos.Student;
import com.javalabs.javalabs.helper.ObjectHandler;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentService {

    @Value("${student.path}")
    private String studentPath;

    private final ObjectHandler objectHandler = ObjectHandler.getInstance();

    public void addStudent(Student student) {
        try {
            FileOutputStream fs = new FileOutputStream(studentPath + student.getId());
            ObjectOutputStream os = new ObjectOutputStream(fs);
            objectHandler.writePerson(os, student);

            os.close();
            fs.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public boolean removeStudent(Integer personId) {
        File student = new File(studentPath + personId);
        return student.delete();
    }

    public Student getStudent(Integer studentId) {
        try {
            FileInputStream fis = new FileInputStream(studentPath + studentId);
            ObjectInputStream is = new ObjectInputStream(fis);
            Student student = objectHandler.readPerson(is);

            is.close();
            fis.close();

            return student;

        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean exists(Integer studentId) {
        File file = new File(studentPath + studentId);
        return file.exists();
    }

    public void removeAll() throws IOException {
        FileUtils.cleanDirectory(new File(studentPath));
    }

    public List<Student> getAll() {
        List<Student> students;
        File directory = new File(studentPath);

        students = Arrays.stream(directory.list()).map(id -> getStudent(Integer.parseInt(id)))
                .collect(Collectors.toList());

        return students;
    }
}
