package com.javalabs.javalabs.service;

import com.javalabs.javalabs.dtos.Teacher;
import com.javalabs.javalabs.helper.ObjectHandler;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeacherService {
    @Value("${teacher.path}")
    private String teacherPath;

    private final ObjectHandler objectHandler = ObjectHandler.getInstance();

    public void addTeacher(Teacher teacher) {
        try {
            FileOutputStream fs = new FileOutputStream(teacherPath + teacher.getId());
            ObjectOutputStream os = new ObjectOutputStream(fs);
            objectHandler.writePerson(os, teacher);

            os.close();
            fs.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public boolean removeTeacher(Integer personId) {
        File teacher = new File(teacherPath + personId);
        return teacher.delete();
    }

    public Teacher getTeacher(Integer studentId) {
        try {
            FileInputStream fis = new FileInputStream(teacherPath + studentId);
            ObjectInputStream is = new ObjectInputStream(fis);
            Teacher teacher = objectHandler.readPerson(is);

            is.close();
            fis.close();

            return teacher;

        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean exists(Integer studentId) {
        File file = new File(teacherPath + studentId);
        return file.exists();
    }

    public void removeAll() throws IOException {
        FileUtils.cleanDirectory(new File(teacherPath));
    }

    public List<Teacher> getAll() {
        List<Teacher> teachers;
        File directory = new File(teacherPath);

        teachers = Arrays.stream(directory.list()).map(id -> getTeacher(Integer.parseInt(id)))
                .collect(Collectors.toList());

        return teachers;
    }
}
